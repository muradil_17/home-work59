import axios from 'axios';

const realBase = axios.create({
    baseURL: "https://real-madrid-68cb7.firebaseio.com/",
});

export default realBase;