import React from 'react';
import './Achievements.css'

const Achievements = (props) => {
    return (
        <div className='Achievements'>
            <p><span>{props.name}</span>{props.years}</p>
            <img src={props.img} alt="Espana"/>
        </div>
    );
};

export default Achievements;