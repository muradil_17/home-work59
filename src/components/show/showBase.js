import React, {Component} from 'react';
import axios from './../../axiosbase'
import './showStyle.css'
import Spinner from "../Spinner/spinner";

class ShowBase extends Component {
    state = {
        image: '',
        name: '',
        number: '',
        position: '',
        loading: false
    };

    change = event =>{
      this.setState({[event.target.name]: event.target.value})
    };


    submit = (event) =>{
        event.preventDefault();
        this.setState({loading: true});
        if (this.state.name === '' && this.state.number === '' && this.state.position === ''){
            return false
        }else {
            const Player ={
                name: this.state.name,
                number: this.state.number,
                position: this.state.position,
                image: this.state.image
            };

            axios.post('/player.json', Player).finally(()=>{
                this.setState({loading: false});
                this.props.history.push('/secondPage');
            })
        }
    };

    render() {
        let form = (
            <div className='createPlayer'>
                <div className="text">
                    <h1>Create Player</h1>
                </div>
                <form className="Form">
                    <input type="text" placeholder="Фото" name="image" onChange={this.change} value={this.state.image} className='Input'/>
                    <input type="text" placeholder="Ваше имя" name="name" onChange={this.change} value={this.state.name} className='Input'/>
                    <input type="text" placeholder="Цифра на спине" name="number" onChange={this.change} value={this.state.number} className='Input'/>
                    <input type="text" placeholder="Позиция" name="position" onChange={this.change} value={this.state.position} className='Input'/>
                    <button onClick={this.submit} className='button'>Add Player</button>
                </form>
            </div>
        );

        if (this.state.loading){
            form = <Spinner/>
        }
        return (
            <div className='createPlayer'>
                {form}
            </div>
        );
    }
}

export default ShowBase;