import React, {Component} from 'react';
import {Button, Card, CardBody, CardImg, CardSubtitle,  CardTitle} from "reactstrap";
import axios from './../../axiosbase'

import './realPersonals.css'

class Coach extends Component {
    state ={
      name: '',
      year: '',
      position: '',
      image: '',
      response: null
    };

    componentDidMount() {
        axios.get('/coach.json').then(response =>{
            const coach = response.data;
            this.setState({name: coach.name, year: coach.years, position: coach.position, image: coach.image, coach: coach})
        })
    }

    render() {
        return (

            <div className='CoachCard'>
                {this.state.coach ?<Card sm='6'>
                    <CardImg top width="300px" src={this.state.image} alt="Card image cap" />
                    <CardBody>
                        <CardTitle><h3>{this.state.name}</h3></CardTitle>
                        <CardSubtitle><p>{this.state.position}</p></CardSubtitle>
                        <Button>Подробнее</Button>
                    </CardBody>
                </Card>: null}
            </div>
        );
    }
}

export default Coach;