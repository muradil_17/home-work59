import React, {Component} from 'react';
import axios from './../../axiosbase'
import './seconPage.css'
import RealPersonals from "./RealPersonals";

class SecondPage extends Component {
    state={
        player : [],
        loading: true,
        response: null
    };

    componentDidMount() {
        axios.get('/player.json').then(response=>{
            const createFetch =[];
            for(let key in response.data){
                createFetch.push({...response.data[key], id:key})
            }
            this.setState({player: createFetch, createFetch: createFetch})
        })
    }

    render() {
        let player = this.state.player.map((player, i) =>{
            return(
                <div key={i} className='result'>
                    <img src={player.image} alt="photoUser"/>
                    <h3>{player.name}</h3>
                    {/*<h1>Number: <span>{player.number}</span></h1>*/}
                    <p>{player.position}</p>
                </div>
            )
        });
        return (
            <div className='resultShow'>
                <RealPersonals/>
                <div className="createPlayerUser">
                    <div className="text-user">
                        <h1>Игроки созданы вами</h1>
                    </div>
                    <div className="create-user">
                        {this.state.createFetch ? player: null}
                    </div>
                </div>
            </div>
        );
    }
}

export default SecondPage;