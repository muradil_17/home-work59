import React, {Component} from 'react';
import {Button, Card, CardBody, CardImg, CardSubtitle,  CardTitle} from "reactstrap";
import axios from './../../axiosbase'

import './realPersonals.css'
import Coach from "./Coach";

class RealPersonals extends Component {
    state ={
        players : [],
        image: '',
        name: '',
        number: '',
        position: '',
        nation: '',
        response: null
    };

    componentDidMount() {
        axios.get('/players.json').then(response=>{
            const realPlayers =[];
            for(let key in response.data){
                realPlayers.push({...response.data[key], id:key})
            }
            this.setState({players: realPlayers, realPlayers: realPlayers})
        })
    }

    render() {
        let realPlayers = this.state.players.map((player, j)=>{
            return (
                <div key={j} className="resultCard">
                    <Card sm='6'>
                        <CardImg top width="300px" height="350px" src={player.image} alt="Card image cap" />
                        <CardBody>
                            <CardTitle><h3>{player.name}</h3></CardTitle>
                            <CardSubtitle><p>{player.position}</p></CardSubtitle>
                            <Button>Подробнее</Button>
                        </CardBody>
                    </Card>
                </div>
            )
        });
        return (
            <div className='bigContent'>
                <div className="text-coach">
                    <h1>Тренер</h1>
                </div>
                <Coach/>
                <div className="text-team">
                    <h1>Команда Реал Мдрида</h1>
                </div>
                <div className='card'>
                    {this.state.realPlayers ? realPlayers: null}
                </div>
            </div>
        );
    }
}

export default RealPersonals;