import React, {Component, Fragment} from 'react';
import './App.css';
import Home from "./components/Home/Home";
import logo from "./images/realLogo.png";
import {BrowserRouter, NavLink, Route, Switch} from "react-router-dom";
import ShowBase from "./components/show/showBase";
import SecondPage from "./components/page2/secondPage";



class App extends Component{


  render() {
    return (
      <Fragment>
        <BrowserRouter>
          <div className="header">
            <div className="logo">
              <a href="/"><img src={logo} alt="logo"/></a>
              <p>hala madrid</p>
            </div>
            <div className="navText">
              <h1>
                <NavLink to='/' exact activeClassName='active'>Главная</NavLink>
              </h1>
              <h1>
                <NavLink to='/createPlayer' exact activeClassName='active'>Создания игрока</NavLink>
              </h1>
              <h1>
                <NavLink to='/secondPage' exact activeClassName="active" >Персоналы</NavLink>
              </h1>
            </div>
          </div>
          <Switch>
            <Route path='/createPlayer' component={ShowBase}/>
            <Route path='/secondPage' component={SecondPage}/>
            <Route path='/' component={Home}/>
          </Switch>
        </BrowserRouter>
      </Fragment>
  );
  }


}

export default App;
